const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.givenProvider.HttpProvider("http://localhost:7545"));

const contractABI = [{
        "inputs": [],
        "name": "CompterVotes",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [],
        "name": "debutPropositionsInscription",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [],
        "name": "debutSessionVote",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [],
        "stateMutability": "nonpayable",
        "type": "constructor"
    },
    {
        "anonymous": false,
        "inputs": [{
            "indexed": false,
            "internalType": "address",
            "name": "voterAddress",
            "type": "address"
        }],
        "name": "ElecteurInscrit",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [{
            "indexed": false,
            "internalType": "uint256",
            "name": "proposalId",
            "type": "uint256"
        }],
        "name": "EnregistrementProposition",
        "type": "event"
    },
    {
        "inputs": [{
            "internalType": "address",
            "name": "_voter",
            "type": "address"
        }],
        "name": "enregistrerElecteurs",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [{
            "internalType": "string",
            "name": "_description",
            "type": "string"
        }],
        "name": "enregistrerProposition",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [],
        "name": "finPropositionsInscription",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [],
        "name": "FinSessionVote",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "anonymous": false,
        "inputs": [{
                "indexed": false,
                "internalType": "enum Voting.WorkflowStatus",
                "name": "previousStatus",
                "type": "uint8"
            },
            {
                "indexed": false,
                "internalType": "enum Voting.WorkflowStatus",
                "name": "newStatus",
                "type": "uint8"
            }
        ],
        "name": "StatusChange",
        "type": "event"
    },
    {
        "inputs": [{
            "internalType": "uint256",
            "name": "_proposalId",
            "type": "uint256"
        }],
        "name": "vote",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "anonymous": false,
        "inputs": [{
                "indexed": false,
                "internalType": "address",
                "name": "voter",
                "type": "address"
            },
            {
                "indexed": false,
                "internalType": "uint256",
                "name": "proposalId",
                "type": "uint256"
            }
        ],
        "name": "Voted",
        "type": "event"
    },
    {
        "inputs": [],
        "name": "admin",
        "outputs": [{
            "internalType": "address",
            "name": "",
            "type": "address"
        }],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [],
        "name": "getWinner",
        "outputs": [{
            "internalType": "string",
            "name": "",
            "type": "string"
        }],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [],
        "name": "propositionGagnantId",
        "outputs": [{
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
        }],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [{
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
        }],
        "name": "propositions",
        "outputs": [{
                "internalType": "string",
                "name": "description",
                "type": "string"
            },
            {
                "internalType": "uint256",
                "name": "voteCount",
                "type": "uint256"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [],
        "name": "statut",
        "outputs": [{
            "internalType": "enum Voting.WorkflowStatus",
            "name": "",
            "type": "uint8"
        }],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [{
            "internalType": "address",
            "name": "",
            "type": "address"
        }],
        "name": "voters",
        "outputs": [{
                "internalType": "bool",
                "name": "isRegistered",
                "type": "bool"
            },
            {
                "internalType": "bool",
                "name": "hasVoted",
                "type": "bool"
            },
            {
                "internalType": "uint256",
                "name": "votedProposalId",
                "type": "uint256"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    }
];
const contractAddress = '0x38c6604f2b24877780d01195203D9eCf7405c0Ae'; // Adresse de déploiement de votre contrat

const votingContract = new web3.eth.Contract(contractABI, contractAddress);

let currentAccount = web3.eth.getAccounts()[0];
console.log(currentAccount)

document.getElementById('submitWallet').addEventListener('click', () => {
    currentAccount = document.getElementById('walletAddress').value;
    // Vous pouvez maintenant utiliser `currentAccount` pour interagir avec le contrat
    console.log(currentAccount)
});

document.getElementById('registerVoter').addEventListener('click', async() => {
    const accountToRegister = document.getElementById('voterAddress').value;
    await votingContract.methods.enregistrerElecteurs(accountToRegister).send({ from: currentAccount });
});

document.getElementById('startProposals').addEventListener('click', async() => {
    await votingContract.methods.debutPropositionsInscription().send({ from: currentAccount });
});

document.getElementById('submitProposal').addEventListener('click', async() => {
    const description = document.getElementById('proposalDescription').value;
    await votingContract.methods.enregistrerProposition(description).send({ from: currentAccount });
});

document.getElementById('endProposals').addEventListener('click', async() => {
    await votingContract.methods.finPropositionsInscription().send({ from: currentAccount });
});

document.getElementById('startVoting').addEventListener('click', async() => {
    await votingContract.methods.debutSessionVote().send({ from: currentAccount });
});

document.getElementById('vote').addEventListener('click', async() => {
    const proposalId = document.getElementById('proposalId').value;
    await votingContract.methods.vote(proposalId).send({ from: currentAccount });
});

document.getElementById('endVoting').addEventListener('click', async() => {
    await votingContract.methods.FinSessionVote().send({ from: currentAccount });
});

document.getElementById('countVotes').addEventListener('click', async() => {
    await votingContract.methods.CompterVotes().send({ from: currentAccount });
});

document.getElementById('getWinner').addEventListener('click', async() => {
    const winnerDescription = await votingContract.methods.getWinner().call();
    document.getElementById('winnerDescription').innerText = `Le gagnant est : ${winnerDescription}`;
});